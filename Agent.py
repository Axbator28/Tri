from __future__ import division
import random
import sys

class Agent:

    depots = 0
    prises = 0

    def __init__(self, x, y, grid, t):
        self.memoire = ["None"] *t
        self.x=x
        self.y = y
        self.t=t
        self.objet = "vide"
        self.grille = grid

    def checkObject(self):
        rep = []
        #print("agent en (" + str(self.x) + "," + str(self.y) + ")")
        for i in range(self.x-1, self.x+2):
            for j in range(self.y-1, self.y+2):
                if i>=0 and i<self.grille.x and j>=0 and j<self.grille.y and(i==self.x or j == self.y):
                    if self.grille.grid[j][i] != "agen" and self.grille.grid[j][i]!= "vide":
        #                print("peut prendre objet en " + str(i) + "," + str(j))
                        rep.append([i,j])
        return rep

    def takeObject(self):
        choix = self.checkObject()
        if(len(choix) == 0):
            pass
        probas = []
        cpt = 0.0
        for i in range(len(choix)):
            cpt += self.calculProbaPrise(choix[i][0], choix[i][1])
            probas.append(cpt)
            #print("choix prise : " + str(choix[i][0]) +"," + str(choix[i][1]) + " le cpt :" + str(cpt))
        rnd = random.random()

        #print(rnd)
        for i in range(len(choix)):
            if rnd < probas[i]:
                #print("a choisi : " + str(choix[i][0]) + "," + str(choix[i][1]))
                self.objet = self.grille.grid[choix[i][1]][choix[i][0]]
                self.ajoutMemoire(self.objet)
                if self.objet != "vide":
                    self.prises += 1
                self.grille.grid[choix[i][1]][choix[i][0]]="vide"


                return 1
        self.ajoutMemoire(self.objet)

        return 2

    def ajoutMemoire(self, type):
         if len(self.memoire) < self.t:
             self.memoire.append(type)
         else:
            for i in range(0,self.t-2):
                self.memoire[i] = self.memoire[i+1]
            self.memoire[self.t-1] = type

    def calculProbaPrise(self, x,y):
        type = self.grille.grid[y][x]
        size = 0
        nb = 0
        for i in range(len(self.memoire)):
            if(self.memoire[i]!="Vide"):
                size+=1
                if(self.memoire[i] == type):
                   nb +=1
                else:
                    nb += self.grille.erreur
        if(size==0):
            fp = 0
        else:
            fp = nb/size
            #print("fp = " + str(fp))
        return (self.grille.Kp/(self.grille.Kp + fp))**2

    def calculProbaDepot(self):
        type = self.objet
        size = 0
        nb = 0
        #Size est le nombre de cases "existantes" autour de l'objet
        #nb le nombre d'objet du meme type que celui que l'on veut deposer dans le voisinage.
        for i in range(self.x-1,self.x+2):
            for j in range(self.y-1, self.y+2):
                if(i>=0 and i<self.grille.x and j>=0 and j<self.grille.y):
          #          print("dans proba, i =" + str(i) + "j =" + str(j))
                    if(i!=self.x or j!= self.y):
                        size+=1
                        if (self.grille.grid[j][i] == self.objet):
                            rnd = random.random()
                            print(str(rnd))
                            if rnd > self.grille.erreur:
                                print("tttttttttt")
                                nb += 1

        #print("size = :" + str(size))
        if(size==0):
            fd = 0
        else:
            fd = nb/size
         #   print("fd :" + str(fd))
        return (fd/(self.grille.Kd + fd))**2

    def depotObj(self):
        casesVidesLoin=[]
        casesVides=[]
        #print("Depot")
        #check cases alentours

        #On parcourt les 8 cases autour (en pensant bien a retirer celle ou l'on est.
        for i in range(self.x-1,self.x+2):
            for j in range(self.y-1, self.y+2):
                if(i>=0 and i<self.grille.x and j>=0 and j<self.grille.y):
                    #Cas des 4 cases en diago
                    if(i!=self.x and j!= self.y):
                        if (self.grille.grid[j][i] == "vide"):
                            casesVidesLoin.append([i, j])
                    #Cas des 4 cases verti/hori -> Possibilite de deplacement donc on les traite differament
                    else:
                        if(i!=self.x or j!= self.y):
                            if (self.grille.grid[j][i] == "vide"):
                                casesVides.append([i,j])
                                casesVidesLoin.append([i, j])

        if len(casesVides) > 0:
            # si il y au moins 2 cases proches vides, on gere toutes les cases de facon identique
            if len(casesVides)-len(casesVidesLoin) > 1:
                echantillon = casesVidesLoin
            # si il n'y a qu'une case proche vide on la protege en utilisnt l'autre echantillon pour que l'agent puisse bouger.
            else:
                echantillon = casesVidesLoin
            #print("echantillon")
            #for i in range(0, len(echantillon)):
                #print("[" + str(echantillon[i][0]) + "," + str(echantillon[i][1]) + "]")
            #print(echantillon)
            choix = random.choice(echantillon)
            self.grille.grid[choix[1]][choix[0]] = self.objet
            self.objet = "vide"
            self.depots += 1
            #print("choix :[" + str(choix[0]) + "," + str(choix[1]) + "]")
            return 1
        else:
            return 0


    def Move(self):
        #print("memoire :")
        #for i in range(0,10):
        #   print(self.memoire[i])
        rnd = random.random()
        direc = []
        chosen = ''
        dist = self.grille.i
        if self.x+dist<self.grille.x:
            if self.grille.grid[self.y][self.x+dist]=="vide":
                direc.append('E')
        #        #print("y = " + str(self.y) +" x = " + str(self.x+dist))
        if self.x-dist>=0:
            if self.grille.grid[self.y][self.x - dist]=="vide":
                direc.append('O')
         #       #print("y = " + str(self.y) + " x = " + str(self.x - dist))
        if self.y-dist>=0:
            if self.grille.grid[self.y -dist][self.x]=="vide":
                direc.append('N')
          #      print("y = " + str(self.y - dist) + " x = " + str(self.x))
        if self.y+dist<self.grille.y:
            dist = self.grille.i
            if self.grille.grid[self.y + dist][self.x]=="vide":
                direc.append('S')
           #     print("y = " + str(self.y + dist) + " x = " + str(self.x))


        rnd2 = random.random()
        chosen = False
        for i in range(0,len(direc)):
            if rnd2 < (i+1)/(len(direc)) and chosen == False:
                chosen = direc[i]
            #    print(chosen)
                break

        if chosen != False:
            self.grille.grid[self.y][self.x] = "vide"
            # print(str(self.y) + "," + str(self.x))
            if chosen == 'E':
                self.x += dist
            if chosen == 'O':
                self.x -= dist
            if chosen == 'N':
                self.y -= dist
            if chosen == 'S':
                self.y += dist
            self.grille.grid[self.y][self.x] = "agen"
        actionDone = False
        if self.objet!="vide":
            rnd3 = random.random()
            #On pose l'objet en fonction d'une proba calculee en fonction des objets du voisinage
            #print(self.calculProbaDepot())
            if self.depotObj2()!=0:
                #self.depotObj()
                actionDone = True
        if self.objet=="vide" and actionDone == False:
            self.takeObject2()

    def depotObj2(self):
        casesVidesLoin = []
        # print("Depot")
        # check cases alentours

        # On parcourt les 8 cases autour (en pensant bien a retirer celle ou l'on est.
        for i in range(self.x - 1, self.x + 2):
            for j in range(self.y - 1, self.y + 2):
                if (i >= 0 and i < self.grille.x and j >= 0 and j < self.grille.y):
                    # Cas des 4 cases en diago
                    if (i != self.x and j != self.y):
                        if (self.grille.grid[j][i] == "vide"):
                            casesVidesLoin.append([i, j])
                    # Cas des 4 cases verti/hori -> Possibilite de deplacement donc on les traite differament
                    else:
                        if (i != self.x or j != self.y):
                            if (self.grille.grid[j][i] == "vide"):
                                casesVidesLoin.append([i, j])

        if len(casesVidesLoin) > 0:
            echantillon = casesVidesLoin
            #print("echantillon")
            #for i in range(0, len(echantillon)):
            #    print("[" + str(echantillon[i][0]) + "," + str(echantillon[i][1]) + "]")
            #print("boucle")
            for i in range(0,len(casesVidesLoin)):
                rnd = random.randint(0, len(echantillon) - 1)
                a = echantillon[rnd]
                echantillon.pop(rnd)
                prob = self.grille.propaDepot(a[0], a[1], self.objet)
                #print("[" + str(a[0]) + "," + str(a[1]) + "] " + str(prob))
                rnd2 = random.random()
                #print("rnd2 =" + str(rnd2))
                if(rnd2<prob):
                    self.grille.grid[a[1]][a[0]] = self.objet
                    self.objet = "vide"
                    return 1
            return 0
        else:
            return 0

    def takeObject2(self):
        choix = self.checkObject()
        if(len(choix) == 0):
            pass
        echantillon = choix
        for i in range(len(choix)):
            rnd = random.randint(0, len(echantillon) - 1)
            a = echantillon[rnd]
            echantillon.pop(rnd)
            prob = self.calculProbaPrise(a[0], a[1])
            rnd2 = random.random()
            if(rnd2<prob):
                #print("a choisi : " + str(a[0]) + "," + str(a[1]))
                self.objet = self.grille.grid[a[1]][a[0]]
                self.ajoutMemoire(self.objet)
                if self.objet != "vide":
                    self.prises += 1
                self.grille.grid[a[1]][a[0]] = "vide"
                self.ajoutMemoire(self.objet)
                return 1
        return 2
