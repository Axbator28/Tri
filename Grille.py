from __future__ import print_function
from __future__ import division
from Agent import Agent
import sys
import random



class Grille:

    def __init__(self, x, y, Kp, Kd, i, t, erreur = 0):
        self.grid = [["vide" for h in range(x)] for j in range(y)]
        self.i = i
        self.t = t
        self.x = x
        self.y = y
        self.Kp = Kp
        self.Kd = Kd
        self.agents = []
        self.erreur = erreur
        # print(self.x)

    def changeCase(self, string, x, y):
        self.grid[x][y]  = string

    def addAgent(self, x, y):
        if self.grid[y][x]=="vide":
            agent = Agent(x, y, self, self.t)
            self.agents.append(agent)
            self.grid[y][x] = "agen"

    def addObject(self, x, y, type):
        if self.grid[y][x]=="vide":
            self.grid[y][x] = type

    def toString(self):
        for i in range(0, self.y):
            for j in range(0, self.x):
                sys.stdout.write(self.grid[i][j] + " ")
                sys.stdout.flush()
            print()

    def fulfill(self, nbAgents, nbObjets):
        keys = [x for x in range(self.x * self.y)]
        for i in range(nbAgents):
            rnd =random.randint(0, len(keys)-1)
            a = keys[rnd]
            keys.pop(rnd)
            self.addAgent(a//self.x, a % self.y)
        for i in range(nbObjets):
            rnd = random.randint(0, len(keys)-1)
            a = keys[rnd]
            keys.pop(rnd)
            self.addObject(a // self.x, a % self.y, "objA")
        for i in range(nbObjets):
            rnd = random.randint(0, len(keys)-1)
            a = keys[rnd]
            keys.pop(rnd)
            self.addObject(a // self.x, a % self.y, "objB")
        self.toString()

    def getPrises(self):
        prises = 0
        for i in range(len(self.agents)):
            prises += self.agents[i].prises
        return prises

    def getDepots(self):
        depots = 0
        for i in range(len(self.agents)):
            depots += self.agents[i].depots
        return depots
    def printNumberAgent(self):
        nb = 0
        for i in range(0, self.y):
            for j in range(0, self.x):
                if self.grid[i][j] == "agen":
                    nb += 1
        print("nombre d'agents: " + str(nb))

    def getAgent(self, x, y):
        n = len(self.agents)
        for i in range(0,n):
            if self.agents[i].y == x and self.agents[i].x == y:
                return self.agents[i]
        return False

    def getEvaluation(self, totalObjets):

        sommeProportionVoisins = 0
        sommeProportionObjets = 0
        for i in range(0, self.y):
            for j in range(0, self.x):
                if self.grid[i][j] != "agen" and self.grid[i][j] != "vide":
                    nbObjIdentiques = 0
                    nbVoisins = 0
                    nbObjets = 0
                    obj = self.grid[i][j]
                    for m in range(i-1, i+2):
                        for n in range(j-1, j+2):
                            if m >= 0 and m < self.y and n >= 0 and n < self.x:
                                if self.grid[m][n] == obj:
                                    nbObjIdentiques += 1
                                nbVoisins += 1
                                if self.grid[m][n] != "agen" and self.grid[m][n] != "vide":
                                    nbObjets += 1
                    if nbVoisins != 1:
                        sommeProportionVoisins += (nbObjIdentiques-1) / (nbVoisins-1)
                    if nbObjets != 1:
                        sommeProportionObjets += (nbObjIdentiques-1) / (nbObjets-1)

        if totalObjets != 0:
            return [sommeProportionVoisins / totalObjets, sommeProportionObjets / totalObjets]
        else:
            return [-1, -1]

    def caseAdjacenteMemeObjet(self, i,j,obj):
        for x in range(i-1,i+2):
            for y in range(j-1,j+2):
                if(x>=0 and y >=0 and x< self.x and y< self.y and self.grid[y][x]==obj and(x!= i or y!=j)):
                    return True
            return False

    def propaDepot(self, i, j, obj):
        type = obj
        size = 0
        nb = 0
        # Size est le nombre de cases "existantes" autour de l'objet
        # nb le nombre d'objet du meme type que celui que l'on veut deposer dans le voisinage.
        for x in range(i - 1, i + 2):
            for y in range(j - 1, j + 2):
                if (x >= 0 and x <= self.x and y >= 0 and y <= self.y):
                    size += 1
                    if ((x != i or y != j) and x < self.x and y < self.y):
                        if (self.grid[y][x] == type):
                            #print("dans proba, x =" + str(x) + "y =" + str(y))
                            nb += 1
                        elif(self.grid[y][x] != "vide"):
                            nb +=self.erreur


        #print("size = :" + str(size))
        #print("nb = :" + str(nb))
        if (size == 0 or nb<0):
            fd = 0
        else:
            fd = nb / 8
        #print("fd :" + str(fd))
        return (fd / (self.Kd + fd)) ** 2