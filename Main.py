from tkinter import *
import random
from Grille import Grille
from matplotlib import pyplot

taille = 50
Kp = 0.3
Kd = 0.3
i = 1
d = 10
erreur = 0.1
nbAgents = 20
nbObjets = 200

grille = Grille(taille, taille, Kp, Kd, i, d, erreur)
global labels
labels = [[0 for h in range(taille)] for j in range(taille)]
evaluationsVoisins = {}
evaluationsObjets = {}
grille.fulfill(nbAgents, nbObjets)

print
grille.toString()

def iterer():
    for j in range(0,10000):
        for i in range(0, nbAgents):
            grille.agents[i].Move()


def start():
    iterer()
    clear()
    drawAllElements()
    evaluationsVoisins[len(evaluationsVoisins)] = grille.getEvaluation(400)[0]
    evaluationsObjets[len(evaluationsObjets)] = grille.getEvaluation(400)[1]
    pyplot.plot(evaluationsVoisins.keys(), evaluationsVoisins.values(), color= "green")
    pyplot.plot(evaluationsObjets.keys(), evaluationsObjets.values(), color="red")
    pyplot.show()


def clear():
    Canevas.delete(ALL)

def drawElement(x, y, color):
    Canevas.create_oval(x*10, y*10, (x+1)*10, (y+1)*10, outline=color, fill=color)

def drawAllElements():
    for i in range(0,taille):
        for j in range(0,taille):
            color = "white"
            if grille.grid[i][j] == "agen":
                if grille.getAgent(i,j).objet == "vide":
                    color = "black"
                else:
                    color = "green"
            if grille.grid[i][j] == "objA":
                color = "red"
            if grille.grid[i][j] == "objB":
                color = "yellow"
            drawElement(j, i, color)


# Creation de la fenetre principale (main window)
Mafenetre = Tk()
Mafenetre.title('Tri')

# Creation d'un widget Canvas (zone graphique)
Largeur = taille * 10
Hauteur = taille * 10
Canevas = Canvas(Mafenetre, width = Largeur, height =Hauteur, bg ='white')
Canevas.pack(padx =5, pady =5)
drawAllElements()

BoutonGo = Button(Mafenetre, text ='Iterer 10000 fois', command = start)
BoutonGo.pack(side = LEFT, padx = 10, pady = 10)

Mafenetre.mainloop()

